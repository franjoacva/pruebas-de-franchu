package com.memorandum.webservice.entity;

public class Redsys {
	Long Id;
	String Ds_SignatureVersion;
	String Ds_MerchantParameters;
	String Ds_Signature;
	
	
	
	public Redsys(Long id, String ds_SignatureVersion, String ds_MerchantParameters, String ds_Signature) {
		super();
		Id = id;
		Ds_SignatureVersion = ds_SignatureVersion;
		Ds_MerchantParameters = ds_MerchantParameters;
		Ds_Signature = ds_Signature;
	}
	
	
	

	public Long getId() {
		return Id;
	}
	
	public void setId(Long id) {
		Id = id;
	}
	
	public String getDs_SignatureVersion() {
		return Ds_SignatureVersion;
	}
	
	public void setDs_SignatureVersion(String ds_SignatureVersion) {
		Ds_SignatureVersion = ds_SignatureVersion;
	}
	
	public String getDs_MerchantParameters() {
		return Ds_MerchantParameters;
	}
	
	public void setDs_MerchantParameters(String ds_MerchantParameters) {
		Ds_MerchantParameters = ds_MerchantParameters;
	}
	
	public String getDs_Signature() {
		return Ds_Signature;
	}
	
	public void setDs_Signature(String ds_Signature) {
		Ds_Signature = ds_Signature;
	}




	@Override
	public String toString() {
		return "Redsys [Id=" + Id + ", Ds_SignatureVersion=" + Ds_SignatureVersion + ", Ds_MerchantParameters="
				+ Ds_MerchantParameters + ", Ds_Signature=" + Ds_Signature + "]";
	}
	
	
	
	
}
