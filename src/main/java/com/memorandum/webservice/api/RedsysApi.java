package com.memorandum.webservice.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.memorandum.webservice.entity.Redsys;

@RestController
public class RedsysApi {
	
	@RequestMapping(value="/redsys", method=RequestMethod.GET)
	public Redsys getById(){
		
        return new Redsys(10L, "SignatureVersion", "Ds_MerchantParameters", "Ds_Signature");
    }
}
