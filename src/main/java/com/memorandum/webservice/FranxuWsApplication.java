package com.memorandum.webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FranxuWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FranxuWsApplication.class, args);
	}

}
